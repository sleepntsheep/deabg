#define _POSIX_C_SOURCE 200809L
#include <string.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include "debugger.h"
#include "log.h"
#include "stb_ds.h"

void debugger_init(debugger_t *db, pid_t pid, char *prog) {
    db->pid = pid;
    db->prog = prog;
    db->bps = NULL;
}

void debugger_set_breakpoint_at(debugger_t *db, intptr_t addr) {
    breakpoint_t bp = {
        .enabled = false,
        .pid = db->pid,
        .addr = addr,
        .saved_data = 0,
    };
    breakpoint_enable(&bp);
    hmput(db->bps, addr, bp);
}

void debugger_continue(debugger_t *db) {
    ptrace(PTRACE_CONT, db->pid, NULL, NULL);
    int status;
    waitpid(db->pid, &status, 0);
}

void debugger_handle_command(debugger_t *db, char *line) {
    char **args = NULL;
    {
        char *p = line, *s = line;
        while (*p) {
            while (*p && *p != ' ') p++;
            char *dup = strndup(s, p - s);
            arrput(args, dup);
            while (*p == ' ') p++;
            s = p;
        }
    }
    char cmd = line[0];
    switch (cmd) {
        case 'c':
            debugger_continue(db);
            break;
        case 'b': {
            if (arrlen(args) < 2) {
                warn("Break need argument!\n"
                        "Ex: > b 0xDEAD");
                break;
            }
            errno = 0;
            intptr_t addr = strtoll(args[1], NULL, 16);
            if (errno != 0) {
                warn("Cannot convert args to address!\n"
                        "Make sure args is in hexadecimal");
                break;
            }
            info("Put a breakpoint at 0x%X", addr);
            debugger_set_breakpoint_at(db, addr);
            break;
                  }
        default:
            warn("Unknown option: %s!", args[1]);
            break;
    }
    for (long i = 0; i < arrlen(args); i++)
        free(args[i]);
    arrfree(args);
}

void debugger_run(debugger_t *db) {
    int status;
    waitpid(db->pid, &status, 0);

    char buf[2048] = { 0 };
    // TODOO: use something like libreadline or linenoise to handle input, rather than this simple fgets
    while (fgets(buf, sizeof buf, stdin) != NULL) {
        *strchr(buf, '\n') = '\0';
        debugger_handle_command(db, buf);
    }
}


