#include "breakpoint.h"
#include <sys/ptrace.h>

void breakpoint_enable(breakpoint_t *b) {
    long data = ptrace(PTRACE_PEEKDATA, b->pid, b->addr, NULL);
    b->saved_data = data & 0xFF;
    /* little endian!! */
    long data_with_int3 = (data & ~0xFF) | 0xcc;
    ptrace(PTRACE_POKEDATA, b->pid, b->addr, data_with_int3);
    b->enabled = true;
}

void breakpoint_disable(breakpoint_t *b) {
    long data = ptrace(PTRACE_PEEKDATA, b->pid, b->addr, NULL);
    long restored = (data & ~0xFF) | b->saved_data;
    ptrace(PTRACE_POKEDATA, b->pid, b->addr, restored);
    b->enabled = false;
}

