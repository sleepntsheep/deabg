
all: deabugger debuggee
srcs := main.c debugger.c breakpoint.c
cflags := #-std=c99 -ggdb# -Wall -Wextra -pedantic

deabugger: *.c
	cc $(srcs) -o deabugger $(cflags)

debuggee:
	cc debuggee.c -o debuggee -g -Wall -no-pie

