#pragma once

#ifndef BREAKPOINT_H_
#define BREAKPOINT_H_
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

typedef struct {
    bool enabled;
    pid_t pid;
    intptr_t addr;
    uint8_t saved_data;
} breakpoint_t;

void breakpoint_enable(breakpoint_t *b);
void breakpoint_disable(breakpoint_t *b);

#endif /* BREAKPOINT_H_ */

