#pragma once

#ifndef DEBUGGER_H_
#define DEBUGGER_H_

#include <sys/types.h>
#include <unistd.h>
#include "breakpoint.h"
#include "stb_ds.h"

typedef struct {
    pid_t pid;
    char *prog;
    struct {
        intptr_t key;
        breakpoint_t value;
    } *bps;
} debugger_t;

void debugger_init(debugger_t *db, pid_t pid, char *prog);
void debugger_set_breakpoint_at(debugger_t *db, intptr_t addr);
void debugger_continue(debugger_t *db);
void debugger_handle_command(debugger_t *db, char *line);
void debugger_run(debugger_t *db);

#endif /* DEBUGGER_H_ */

