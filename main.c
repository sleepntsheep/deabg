#define _POSIX_C_SOURCE 200809L

#include <unistd.h>
#include <stdio.h>
#include <sys/personality.h>
#include <sys/ptrace.h>
#define SHEEP_LOG_IMPLEMENTATION
#include "log.h"
#include "debugger.h"
#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

int main(int argc, char **argv) {
    if (argc != 2) panic("Usage: %s [PROGRAM]", argv[0]);
    pid_t pid = fork();
    debugger_t db;
    debugger_init(&db, pid, argv[1]);

    if (pid == 0) {
        personality(ADDR_NO_RANDOMIZE);
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        execl(argv[1], argv[1], NULL);
    } else {
        info("Started debugging program (pid %d)", pid);
        debugger_run(&db);
    }

    return 0;
}

